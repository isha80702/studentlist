package sheridan.isha.student;

/**
 * This class represents students in our application
 *
 * @author Paul Bonenfant
 */
public class Student {
private String program;
   private String name;
   private String id;

   public Student (String name, String id)
   {
      this.name = name;
      this.id = id;
   }

   public String getName ()
   {
      return name;
   }

   public void setName (String name)
   {
      this.name = name;
   }

   public String getId ()
   {
      return id;
   }

   public void setId (String id)
   {
      this.id = id;
   }
}
    
   

